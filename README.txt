Readme
------

A module for synchronizing a forum or other node type with a netnews server.

TODO
-----

Currently, this module does the basics:

for a configured link between a Drupal forum and a netnews newsgroup,
all new (forum)nodes and comments are sent to the newsgroup, and all new 
posts and followups from the newsgroup are imported into the forum.

A lot remains to be done:
- handle attachments
- strip quoting from netnews messages when possible
- options for how to deal with the owners of netnews messages in the forum
- and more

Credits
-------

The Dutch Open University (OUNL, http://www.ou.nl) for annoying me so much
by their ill-considered move from nntp newsgroups to a web-based forum, that
I decided to see for myself if integrating the two would be feasible.
Google's Summer of Code program (http://code.google.com/summerofcode.html),
for encouraging me to translate my annoyance into a working Drupal module.
My mentor at Drupal, Karoly Negyesi for helping me "into" Drupal, and
Robert Douglass for doing more of that.
Astrid Nijs for pointing out Google's Summer of Code program to me.
My fellow students at OUNL for encouraging me, and testing.

Author
------

Jan Blom <jan.j.blom AT gmailDOTcom>
