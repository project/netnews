Installation
------------

1. Copy this netnews.module to the Drupal modules/ directory. Drupal will
   automatically detect it. create a netnews directory in modules/ and move
   nntpserver.inc there. 

2. Create the SQL table. This depends a little on your system, but the
    most common method is:
      mysql -u username -ppassword drupal < netnews.mysql

3. Enable the netnews module in Drupal, and configure links between
   forums and netnews trough admin/netnews.
      
Author
------

Jan Blom <jan.j.blom AT gmailDOTcom>
